import * as ko from "knockout";

import { Factura } from "../interfaces/factura";
import { facturas } from "../datos/facturas"

class FacturasMV {
    
    facturas: KnockoutObservableArray<Factura> = ko.observableArray(facturas);

    fotos = {
        feliz: "feliz.jpg",
        triste: "triste.jpg"
    }

    rutaFotos: string = "img/";

    formVisible: KnockoutObservable<boolean> = ko.observable(false);
    
    toggleForm(): void {
        this.formVisible(!this.formVisible());
    }
    
    calculaTotalFactura = factura => parseFloat((factura.base * (1 + (factura.iva / 100))).toFixed(2))
    
    calculaIva = factura => (factura.base * factura.iva / 100).toFixed(2)
    
    ingresos: KnockoutComputed<Factura[]> = ko.computed( () => {
        let ingresos: Array<Factura> = ko.utils.arrayFilter(this.facturas(), 
            factura => factura.tipo == "ingreso"
        );
                
        return ingresos;
    })

    gastos: KnockoutComputed<Factura[]> = ko.computed( () => {
        let gastos: Array<Factura> = ko.utils.arrayFilter(this.facturas(), 
            factura => factura.tipo == "gasto"
        );
                
        return gastos;
    })    

    totalGastos: KnockoutComputed<number> = ko.computed( 
        () => parseFloat(this.gastos()
                            .map( factura => this.calculaTotalFactura(factura) )
                            .reduce(
                                (total1, total2) => total1 + total2
                            )
                            .toFixed(2))
    )

    totalIngresos: KnockoutComputed<number> = ko.computed( 
        () => parseFloat(this.ingresos()
                            .map( factura => this.calculaTotalFactura(factura) )
                            .reduce(
                                (total1, total2) => total1 + total2
                            )
                            .toFixed(2))
    )

    balance: KnockoutComputed<number> = ko.computed(
        () => parseFloat((this.totalIngresos() - this.totalGastos()).toFixed(2))
    )

    balancePositivo: KnockoutComputed<boolean> = ko.computed(
        () => this.balance() >= 0
    )

    muchosGastos: KnockoutComputed<boolean> = ko.computed( () => this.gastos().length > 2 );

    caraBalance: KnockoutComputed<string> = ko.computed( 
        () => this.balancePositivo() ? this.rutaFotos + this.fotos.feliz : this.rutaFotos + this.fotos.triste 
    )

}

export let facturasMV: FacturasMV = new FacturasMV();
