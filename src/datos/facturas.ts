import { Factura } from "../interfaces/factura";

export let facturas: Array<Factura> = [
    { numero: "A1200", base: 135, iva: 21, tipo: "ingreso" },
    { numero: "A1201", base: 520, iva: 21, tipo: "ingreso" },
    { numero: "3992", base: 1.75, iva: 7, tipo: "gasto" },
    { numero: "240", base: 16.95, iva: 21, tipo: "gasto" }
]
