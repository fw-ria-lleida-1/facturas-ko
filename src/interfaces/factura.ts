export interface Factura {
    numero: string,
    base: number,
    iva: number,
    tipo: string
}