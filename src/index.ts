import './scss/estilos.scss';
import '../index.html';

import * as ko from "knockout";

import { Factura } from "./interfaces/factura";
import { facturasMV } from "./modelviews/facturas.mv";

ko.applyBindings(facturasMV);

setTimeout(() => {
    facturasMV.facturas.push({
        numero: "999",
        base: 1000,
        iva: 21,
        tipo: "gasto"
    })
}, 3000);
setTimeout(() => {
    facturasMV.facturas.push({
        numero: "11",
        base: 500,
        iva: 21,
        tipo: "ingreso"
    })
}, 6000);


