// El módulo path lo tenemos por el hecho de tener instalado NodeJS
// Se usa en este fichero para construir rutas
const path = require('path');

module.exports = {
    devtool: 'inline-source-map',  // Para que genere los mapas dentro de los archivos
    entry: ['bootstrap-loader', './src/index.ts'], // Para que funcione el loader de BS, debe estar aquí. El segundo es nuestro script de entrada
    output: {  // Ruta y nombre del chorizo generado
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/dist/'
    },
    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: ['.ts', '.tsx', '.js']
    },
    module: {
        rules: [  // Qué loaders se encargarán de interpretar cada tipo de archivo
            { test: /bootstrap-sass(\\|\/)assets(\\|\/)javascripts(\\|\/)/, loader: 'imports-loader?jQuery=jquery' },  // Para la parte JS de Bootstrap
            { test: /\.scss$/, loader: ['style-loader', 'css-loader', 'sass-loader'] },  // Para los archivos SASS
            { test: /\.woff2?$|\.ttf$|\.eot$|\.svg$/, loader: 'url-loader'},   // Para las tipografías que se cargan desde CSS (p.e, Glyphicons)
            { test: /\.tsx?$/, loader: 'ts-loader' },  // Para los archivos de TypeScript
            { test: /\.html/, loader: 'raw-loader' }   // Para que se recargue nuestro index.html
        ]
    }
}